<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// J
	'jcrop_titre' => 'Crop Image',

	// C
	'cfg_exemple' => 'Exemple',
	'cfg_exemple_explication' => 'Explication de cet exemple',
	'cfg_titre_parametrages' => 'Paramétrages',

	// R
	'recadrer' => 'Recadrer',
	'recadrer_submit' => 'Recadrer',
	// S
	'selectionner_region_obligatoire' => 'Vous devez sélectionner une région avant de valider',
	// T
	'titre_page_configurer_jcrop' => 'Crop Image',
);
